#!/usr/bin/env python
# encoding: utf-8
import torch
import torch.nn as nn
import os

from preprocessing.preprocess import one_hot_to_2D
from preprocessing.format_data import *
from train.trainer import evaluate, to_string, train, predict, plot_confusion_matrix
from models.MLPRNN import MLPRNN


def main():
    directory = r'datasets/'

    train_csv = np.load(directory + r'cullpdb+profile_6133_filtered.npy')
    test_csv = np.load(directory + r'cb513+profile_split.npy')

    train_hhm = np.load(directory + r'hhm_train.npy')  # hhm_train_modify.npy
    test_hhm = np.load(directory + r'hhm_test.npy')  # hhm_test_modify.npy

    max_length = 700
    input_class_number = 21
    output_class_number = 9  # Q3 is 4

    train_one_hot_array, train_profile_array, y_train = load_cul6133_filtered(train_csv)
    test_one_hot_array, test_profile_array, y_test = load_cb513(test_csv)

    y_train = one_hot_to_2D(y_train)
    y_test = one_hot_to_2D(y_test)

    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    x_train_one_hot = torch.tensor(train_one_hot_array, dtype=torch.float32, requires_grad=False).to(device)
    x_test_one_hot = torch.tensor(test_one_hot_array, dtype=torch.float32, requires_grad=False).to(device)

    x_train_profile = torch.tensor(train_profile_array, dtype=torch.float32, requires_grad=False).to(device)
    x_test_profile = torch.tensor(test_profile_array, dtype=torch.float32, requires_grad=False).to(device)

    x_train_hhm = torch.tensor(train_hhm, dtype=torch.float32, requires_grad=False).to(device)
    x_test_hhm = torch.tensor(test_hhm, dtype=torch.float32, requires_grad=False).to(device)

    y_train = torch.tensor(y_train, dtype=torch.int64, requires_grad=False).to(device)
    y_test = torch.tensor(y_test, dtype=torch.int64, requires_grad=False).to(device)

    model = MLPRNN(seq_len=max_length, input_class_number=input_class_number,
                   output_class_number=output_class_number)

    model_name = type(model).__name__
    model = model.to(device, dtype=torch.float32)

    if torch.cuda.device_count() > 1 and device.type == 'cuda':
        model = nn.DataParallel(model)

    batch_size = 64

    train(model, (x_train_profile, x_train_one_hot, x_train_hhm, y_train),
          validation_data=(x_test_profile, x_test_one_hot, x_test_hhm, y_test),
          epochs=200, batch_size=batch_size,
          lr=0.001, lr_decay=0.997, weight_decay=0.,
          shuffle=True, verbose=True, checkpoint=None)

    # evaluate test set
    y_test_hat = predict(model, (x_test_profile, x_test_one_hot, x_test_hhm), batch_size=batch_size)
    y_test_class_hat = y_test_hat.softmax(dim=-1).max(dim=-1).indices
    test_results = evaluate(y_test, y_test_class_hat)
    print(to_string(model_name, *test_results))

    report, matrix = plot_confusion_matrix(y_test, y_test_class_hat)
    print(report, matrix)

    print('End')


if __name__ == '__main__':
    # torch.backends.cudnn.enabled = True
    # torch.backends.cudnn.benchmark = True
    # torch.backends.cudnn.deterministic = True

    main()
