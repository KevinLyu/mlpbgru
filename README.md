# Protein secondary structure prediction with a reductive deep learning method

## Abstract
Protein secondary structures have been identified as the links 
in the physical processes of primary sequences, typically 
random coils, folding into functional tertiary structures that 
enable proteins to involve a variety of biological events in 
life science. Therefore, an efficient protein secondary structure 
predictor is of importance especially when the structure of 
an amino acid sequence fragment is not solved by high-resolution 
experiments, like X-ray crystallography, cryo-electron microscopy, 
and nuclear magnetic resonance spectroscopy, which are usually time 
consuming and expensive. In this paper, a reductive deep 
learning model MLPRNN has been proposed to predict 
either 3-state or 
8-state protein secondary structures. The prediction accuracy by 
the MLPRNN on the publicly available benchmark CB513 data set 
is comparable with those by other state-of-the-art models. More 
importantly, taking into account the reductive architecture, 
MLPRNN could be a baseline for future developments.

## Datasets
Training Set: cb6133-filtered

Testing Set: cb513

The 57 features in datasets are:
[0,22): amino acid residues, with the order of 'A', 'C', 'E', 'D', 'G', 'F', 'I', 'H', 'K', 'M', 'L', 'N', 'Q', 'P', 'S', 'R', 'T', 'W', 'V', 'Y', 'X','NoSeq'

[22,31): Secondary structure labels, with the sequence of 'L', 'B', 'E', 'G', 'I', 'H', 'S', 'T','NoSeq'

[31,33): N- and C- terminals;

[33,35): relative and absolute solvent accessibility, used only for training. (absolute accessibility is thresholded at 15; relative accessibility is normalized by the largest accessibility value in a protein and thresholded at 0.15; original solvent accessibility is computed by DSSP)

[35,57): sequence profile. Note the order of amino acid residues is ACDEFGHIKLMNPQRSTVWXY and it is different from the order for amino acid residues

ALL DATA ABOVE ARE SHARED BY ZHOU:  https://www.princeton.edu/~jzthree/datasets/ICML2014/

We also generated Hidden Markov Model Profile for each amino acid in datasets 
by HH-suite (HH-blits). The dimension of HMM profile matrices are 700 x 20, where 700 represents 
sequences length and 20 represents twenty types of basic amino acid.

## How to Run
(1) Setting up all required python package in requirements.txt

(2) Run start.py