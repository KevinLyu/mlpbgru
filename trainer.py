#!/usr/bin/env python
# encoding: utf-8
import torch
import time
import numpy as np
import pandas as pd
from torch import nn
from torch import optim
from torch.utils import data
from sklearn.metrics import classification_report, confusion_matrix


def to_string(*kwargs):
    _list = [str(kwargs[0])] + ['{:.6f}'.format(_t) for _t in kwargs[1:]]  # parameters to strings
    total = '\t'.join(_list)  # join these strings to another string
    return total


def evaluate(real, prediction):
    # real and prediction: a torch array with shape -> [sequence_number, sequence_length]
    total_hit = real == prediction
    total_accuracy = total_hit.float().mean()

    mask = real > 0
    hit = (real[mask] == prediction[mask])
    hitL = (real[real == 1] == prediction[real == 1])
    hitB = (real[real == 2] == prediction[real == 2])
    hitE = (real[real == 3] == prediction[real == 3])
    hitG = (real[real == 4] == prediction[real == 4])
    hitI = (real[real == 5] == prediction[real == 5])
    hitH = (real[real == 6] == prediction[real == 6])
    hitS = (real[real == 7] == prediction[real == 7])
    hitT = (real[real == 8] == prediction[real == 8])

    # point-wise accuracy
    accuracy = hit.float().mean()
    L_acc = hitL.float().mean()
    B_acc = hitB.float().mean()
    E_acc = hitE.float().mean()
    G_acc = hitG.float().mean()
    I_acc = hitI.float().mean()
    H_acc = hitH.float().mean()
    S_acc = hitS.float().mean()
    T_acc = hitT.float().mean()

    # np_real = real[mask].detach().cpu().numpy()
    # np_prediction = prediction[mask].detach().cpu().numpy()

    # micro, macro, samples
    # p = precision_score(np_real, np_prediction, average='micro', zero_division=0)
    # r = recall_score(np_real, np_prediction, average='micro', zero_division=0)
    # f1 = f1_score(np_real, np_prediction, average='micro', zero_division=0)

    return total_accuracy, accuracy, L_acc, B_acc, E_acc, G_acc, I_acc, H_acc, S_acc, T_acc


def train_confusion_matrix(real, prediction):
    mask = real > 0
    np_real = real[mask].detach().cpu().numpy()
    np_prediction = prediction[mask].detach().cpu().numpy()
    matrix = classification_report(np_real, np_prediction, zero_division=0)
    return matrix


def plot_confusion_matrix(real, prediction):
    mask = real > 0
    np_real = real[mask].detach().cpu().numpy()
    np_prediction = prediction[mask].detach().cpu().numpy()

    report = classification_report(np_real, np_prediction, zero_division=0)
    matrix = confusion_matrix(np_real, np_prediction)

    return report, matrix


class MultipleCrossEntropyLoss(nn.Module):
    def __init__(self, reduction='mean', ignore_zeros=False):
        """
        :param reduction: mean or sum.
        :param ignore_zeros: ignore the loss of 0-th class.
        """
        super(MultipleCrossEntropyLoss, self).__init__()
        self.ignore_zeros = ignore_zeros
        self.reduction = reduction
        self.cross_entropy = nn.CrossEntropyLoss(reduction=self.reduction)

    def forward(self, inputs, targets):
        """
            :param inputs: predicted values (batch_size, sequence_length, input_class_number)
            :param targets: true values (batch_size, sequence_length)
            :return: loss
        """
        inputs = inputs.reshape(-1, inputs.shape[-1])
        targets = targets.reshape(-1)

        if self.ignore_zeros:
            mask = targets > 0
            inputs = inputs[mask]
            targets = targets[mask]

        loss = self.cross_entropy(inputs, targets)
        return loss


@torch.no_grad()
def predict(model, inputs, batch_size=32):
    """inputs: a tuple of torch tensors."""
    model.eval()
    outputs = []
    dataset = data.TensorDataset(*inputs)
    loader = data.DataLoader(dataset=dataset, batch_size=batch_size)
    for step, batch_inputs in enumerate(loader):
        out = model(*batch_inputs).detach()
        outputs.append(out)
    prediction = torch.cat(outputs, dim=0)
    return prediction


def get_result(prediction, r=700):
    q8_digit_list = list('XLBEGIHST')
    q8_result = []
    for i in range(len(prediction)):
        q8_seq = ''
        for j in range(r):
            if prediction[i][j] != 0:
                q8_seq = q8_seq + q8_digit_list[prediction[i][j]]
        q8_result.append(q8_seq)
    return q8_result


def train(model, train_data, validation_data=None,
          epochs=100, batch_size=32, lr=0.001, lr_decay=1., weight_decay=0.001,
          shuffle=False, verbose=False, checkpoint=None):
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)  # Adamax, Adam, RMSprop
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=lr_decay)

    criterion = MultipleCrossEntropyLoss(reduction='mean')

    train_dataset = data.TensorDataset(*train_data)
    train_loader = data.DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=shuffle)

    for epoch in range(1, 1 + epochs):

        # start_time = time.perf_counter()

        model.train()
        for step, batch_train in enumerate(train_loader):
            batch_x, batch_y = batch_train[:-1], batch_train[-1]
            batch_y_hat = model(*batch_x)
            loss = criterion(batch_y_hat, batch_y)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        scheduler.step()

        if checkpoint is not None and checkpoint > 0 and epoch % checkpoint == 0:
            filename = './data/' + 'model.{}.checkpoint'.format(epoch)
            torch.save({'model': model.state_dict()}, filename)

        if not verbose:
            continue

        # evaluate train set
        y_train_hat = predict(model, train_data[:-1], batch_size=batch_size)
        train_loss = criterion(y_train_hat, train_data[-1]).detach()
        y_train_class_hat = y_train_hat.softmax(dim=-1).max(dim=-1).indices
        train_results = evaluate(train_data[-1], y_train_class_hat)

        if validation_data is None:
            print(to_string(epoch, train_loss, *train_results))
        else:
            # evaluate testing set
            y_test_hat = predict(model, validation_data[:-1], batch_size=batch_size)
            test_loss = criterion(y_test_hat, validation_data[-1]).detach()
            y_test_class_hat = y_test_hat.softmax(dim=-1).max(dim=-1).indices
            test_results = evaluate(validation_data[-1], y_test_class_hat)

            '''
            output prediction result every 10 epochs 
            '''
            # if epoch % 1 == 0:
            #     q8_result = get_result(y_test_class_hat)
            #     pred_pd = {'expected': q8_result}
            #     pd.DataFrame(data=pred_pd).to_csv(
            #         r'../outputs/predict_testset/' + 'prediction' + str(epoch) + '.csv', encoding='gbk')

            '''
            Count Time(s)/Epoch
            '''
            # end_time = time.perf_counter()
            # test_time = end_time - start_time
            # print(test_time)

            print(to_string(epoch, train_loss, *train_results, test_loss, *test_results))


def load(model, filename, map_location=None):
    """Load model to cpu."""
    state_dict = torch.load(filename, map_location=map_location)
    model.load_state_dict(state_dict['model'])
    return model
