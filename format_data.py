import numpy as np


def load_cul6133_filtered(data, all_data=1):
    print("Loading training dataset (Cullpdb_filtered)...")

    # reshape dataset
    data = np.reshape(data, (-1, 700, 57))
    # sequence feature
    datahot = data[:, :, 0:21]
    # profile features
    datapssm = data[:, :, 35:56]
    # secondary struture labels
    labels = data[:, :, 22:31]

    # calculate the indexes for each dimension based on all_data input parameter
    data_index = int(len(data) * all_data)

    # get training data
    trainhot = datahot[:data_index]
    trainlabel = labels[:data_index]
    trainpssm = datapssm[:data_index]

    print(trainhot.shape)
    print(trainpssm.shape)
    print(trainlabel.shape)
    print("\n")

    return trainhot, trainpssm, trainlabel


# loading CB513 test dataset
def load_cb513(CB513, all_data=1):
    print("Loading test dataset (CB513)...")

    # reshape dataset
    CB513 = np.reshape(CB513, (-1, 700, 57))
    # sequence feature
    testhot = CB513[:, :, 0:21]
    # profile feature
    testpssm = CB513[:, :, 35:56]
    # secondary struture label
    testlabel = CB513[:, :, 22:31]

    # calculate the indexes for each dimension based on all_data input parameter
    test_data_index = int(len(CB513) * all_data)

    testhot = testhot[:test_data_index]
    testpssm = testpssm[:test_data_index]
    testlabel = testlabel[:test_data_index]

    print(testhot.shape)
    print(testpssm.shape)
    print(testlabel.shape)
    print("\n")

    return testhot, testpssm, testlabel


def load_casp10(casp10_data):
    print("Loading CASP10 dataset...")

    # load protein sequence and profile feature data
    casp10_data_hot = casp10_data['features'][:, :, 0:21]
    casp10_data_pssm = casp10_data['features'][:, :, 21:42]

    # load protein label data
    test_labels = casp10_data['labels'][:, :, 0:9]

    print(casp10_data_hot.shape)
    print(casp10_data_pssm.shape)
    print(test_labels.shape)
    print("\n")

    return casp10_data_hot, casp10_data_pssm, test_labels


# load CASP11 test dataset from cwd
def load_casp11(casp11_data):
    print("Loading CASP11 dataset...")

    # load protein sequence and profile feature data
    casp11_data_hot = casp11_data['features'][:, :, 0:21]
    casp11_data_pssm = casp11_data['features'][:, :, 21:42]
    # load protein label data
    test_labels = casp11_data['labels'][:, :, 0:9]

    print(casp11_data_hot.shape)
    print(casp11_data_pssm.shape)
    print(test_labels.shape)
    print("\n")

    return casp11_data_hot, casp11_data_pssm, test_labels
