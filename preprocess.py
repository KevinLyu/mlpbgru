#!/usr/bin/env python
# encoding: utf-8
import numpy as np
import pandas as pd


def one_hot_to_2D(one_hot_seq):
    q8_list = [1, 2, 3, 4, 5, 6, 7, 8, 0]
    y = np.zeros((len(one_hot_seq), 700))
    i, j = 0, 0
    for i in range(len(one_hot_seq)):
        index_list = []
        for j in range(700):
            index_list.append(q8_list[np.argmax(one_hot_seq[i, j, :])])
        y[i,] = np.array(index_list)

    return y


def one_hot_to_2D_q3(one_hot_seq):
    q3_list = [1, 3, 3, 2, 2, 2, 1, 1, 0]  # origin methods
    # q3_list = [1, 3, 3, 2, 2, 2, 4, 5, 0]
    # q3_list = [1, 3, 3, 2, 1, 2, 1, 1, 0] # CASP reduction
    # q3_list = [1, 1, 3, 1, 1, 2, 1, 1, 0]  # method 2 in paper
    y = np.zeros((len(one_hot_seq), 700))
    i, j = 0, 0
    for i in range(len(one_hot_seq)):
        index_list = []
        for j in range(700):
            index_list.append(q3_list[np.argmax(one_hot_seq[i, j, :])])
        y[i,] = np.array(index_list)

    return y


def residue_to_matrix(residue_sequences, max_length=700):
    """
        Convert several residue sequences to a matrix.
        :param residue_sequences: a string numpy array with shape (sequence_number, 1).
        :param max_length: the maximum length of total sequences.
        :return: a numpy array (num_sequences, max_length)
    """
    residue_list = ['NoSeq'] + list('ACEDGFIHKMLNQPSRTWVYX')
    residue_number = len(residue_list)
    sequence_number = len(residue_sequences)

    matrix = np.zeros((sequence_number, max_length), dtype=np.int)

    for i in range(sequence_number):
        seq = residue_sequences[i, 0]
        seq_array = np.array(list(seq))
        _array = np.zeros_like(seq_array, dtype=np.int)
        for j in range(1, residue_number):
            index = (seq_array == residue_list[j])
            _array[index] = j

        matrix[i, :_array.shape[0]] = _array

    return matrix


def q8_to_matrix(q8_sequences, max_length=700):
    """
        Convert several q8 sequences to a matrix.
        :param q8_sequences: a string numpy array with shape (q8_number, 1).
        :param max_length: the maximum length of total sequences.
        :return: a numpy array (num_sequences, max_length)
    """
    q8_list = ['NoSeq'] + list('LBEGIHST')
    q8_number = len(q8_list)
    sequence_number = len(q8_sequences)

    matrix = np.zeros((sequence_number, max_length), dtype=np.int)

    for i in range(sequence_number):
        seq = q8_sequences[i, 0]
        seq_array = np.array(list(seq))
        _array = np.zeros_like(seq_array, dtype=np.int)
        for j in range(1, q8_number):
            index = (seq_array == q8_list[j])
            _array[index] = j

        matrix[i, :_array.shape[0]] = _array

    return matrix


def one_hot_encoding(sequence_matrix, class_number=22):
    """
        Covert a matrix using one-hot encoding.
        :param sequence_matrix: a numpy array with shape [sequence_number, max_length].
        :param class_number: the number of classes.
        :return: one hot encoded matrix with shape [sequence_number, max_length, class_number].
    """
    eye = np.eye(class_number, dtype=np.int)
    encoding_matrix = eye[sequence_matrix]

    return encoding_matrix


def get_numpy_file(file, bounds=None, r=700, f=57):
    """Get numpy file."""
    arr = np.load(file)

    residue_list = list('ACEDGFIHKMLNQPSRTWVYX') + ['NoSeq']
    q8_list = list('LBEGIHST') + ['NoSeq']
    columns = ["id", "len", "input", "profiles", "expected"]

    if bounds is None: bounds = range(len(arr))

    data = [None for i in bounds]
    for i in bounds:
        seq, q8, profiles = '', '', []
        for j in range(r):
            jf = j * f

            # Residue convert from one-hot to decoded
            residue_onehot = arr[i, jf + 0:jf + 22]
            residue = residue_list[np.argmax(residue_onehot)]

            # Q8 one-hot encoded to decoded structure symbol
            residue_q8_onehot = arr[i, jf + 22:jf + 31]
            residue_q8 = q8_list[np.argmax(residue_q8_onehot)]

            if residue == 'NoSeq': break  # terminating sequence symbol

            nc_terminals = arr[i, jf + 31:jf + 33]  # nc_terminals = [0. 0.]
            sa = arr[i, jf + 33:jf + 35]  # sa = [0. 0.]
            profile = arr[i, jf + 35:jf + 57]  # profile features

            seq += residue  # concat residues into amino acid sequence
            q8 += residue_q8  # concat secondary structure into secondary structure sequence
            profiles.append(profile)

        data[i] = [str(i + 1), len(seq), seq, np.array(profiles), q8]

    return pd.DataFrame(data, columns=columns)


def align_profile_array(profile_array, max_sequence_length=700):
    """
        The last dimension should be equal.
        :param profile_array: a numpy array.
        :param max_sequence_length: the max length a all sequence, and zeros are post padded.
    """
    class_number = profile_array[0].shape[-1]
    aligned_array = np.zeros((len(profile_array), max_sequence_length, class_number))

    for i, profile in enumerate(profile_array):
        aligned_array[i, :profile.shape[0], ] = profile

    return aligned_array


def numpy_df_to_csv(df, csv_file, index_csv_file):
    """Read numpy data from dataframe and write them to csv file."""
    # id list
    id_string = (df.id + ',') * df.len
    id_string = ''.join(id_string)
    id_list = id_string.strip(',').split(',')
    id_pd = pd.DataFrame(columns=['id'], data=id_list)

    # input list
    input_total = ''.join(df.input)
    input_list = list(input_total)
    input_pd = pd.DataFrame(columns=['input'], data=input_list)

    # expected list
    expected_total = ''.join(df.expected)
    expected_list = list(expected_total)
    expected_pd = pd.DataFrame(columns=['expected'], data=expected_list)

    # profiles into a numpy array
    profiles_np = np.concatenate(df.profiles, axis=0)
    profiles_np = profiles_np[:, :-1]
    profiles_df = pd.DataFrame(data=profiles_np, columns=['p' + str(i) for i in range(profiles_np.shape[1])])

    target_df = pd.concat([id_pd, input_pd, expected_pd, profiles_df], axis=1)
    target_df.to_csv(csv_file, index=False)

    index_df = pd.DataFrame(df.len.cumsum().values, columns=['id'])
    index_df.to_csv(index_csv_file, index=False)

    return target_df, index_df


def transform_sequence_data(df, index_df, max_seq_len=700, percentage=1):
    residue_list = list('ACEDGFIHKMLNQPSRTWVYX')
    residue_number = len(residue_list)
    for i, val in enumerate(residue_list):
        df.input.replace(val, i+1, inplace=True)

    q8_list = list('LBEGIHST')
    # q8_number = len(q8_list)
    for i, val in enumerate(q8_list):
        df.expected.replace(val, i+1, inplace=True)

    start_index_df = pd.DataFrame(index_df.shift().fillna(0).astype(int).values, columns=['start'])
    end_index_df = pd.DataFrame(index_df.values, columns=['end'])
    index_pair_df = pd.concat([start_index_df, end_index_df], axis=1)
    index_pair_np = index_pair_df.values

    padded_profiles_np_list, padded_input_list, padded_expected_list = [], [], []
    for row in range(round(index_pair_np.shape[0] * percentage)):
        start, end = index_pair_np[row]

        # profiles
        padded_profiles_np = np.zeros((max_seq_len, residue_number))
        padded_profiles_np[:end-start, :] = df.iloc[start:end, 3:]
        padded_profiles_np_list.append(padded_profiles_np)

        # input
        padded_input = np.zeros(max_seq_len, dtype=np.int)
        padded_input[:end-start] = df.iloc[start:end, 1]
        padded_input_list.append(padded_input)

        # expected
        padded_expected = np.zeros(max_seq_len, dtype=np.int)
        padded_expected[:end-start] = df.iloc[start:end, 2]
        padded_expected_list.append(padded_expected)

    profiles_np = np.array(padded_profiles_np_list)
    inputs_np = np.array(padded_input_list)
    outputs_np = np.array(padded_expected_list)

    return inputs_np, profiles_np, outputs_np


def split_mem_prot(sequence_matrix, path):
    non_mem_prot_df = pd.read_csv(path)
    non_mem_prot_index_list = non_mem_prot_df['id'].values.tolist()
    non_mem_prot_index_list[:] = [x - 1 for x in non_mem_prot_index_list]
    sequence_matrix = np.delete(sequence_matrix, non_mem_prot_index_list, axis=0)
    return sequence_matrix
