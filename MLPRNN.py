#!/usr/bin/env python
# encoding: utf-8
import torch
import torch.nn as nn


class MLPRNN(nn.Module):
    def __init__(self, seq_len, input_class_number=22, output_class_number=9,
                 profile_dim=22, embedding_dim=512, gru_hidden_size=256):
        super(MLPRNN, self).__init__()
        self.seq_len = seq_len
        self.input_class_number = input_class_number
        self.output_class_number = output_class_number
        self.profile_dim = profile_dim

        self.embedding_dim = embedding_dim
        self.gru_hidden_size = gru_hidden_size
        self.combine_dim = self.input_class_number * 1 + 20

        self.gru1 = nn.GRU(input_size=self.embedding_dim, hidden_size=self.gru_hidden_size,
                           bidirectional=True, batch_first=True, num_layers=2, dropout=0.5)

        self.mlp1 = nn.Sequential(
            nn.Linear(self.gru_hidden_size * 2, self.gru_hidden_size),
            nn.ReLU(False),
            nn.Linear(self.gru_hidden_size, self.output_class_number),
            nn.ReLU(False),
            nn.Dropout(0.5)
        )

        self.mlp2 = nn.Sequential(
            nn.Linear(self.combine_dim, self.embedding_dim // 2),
            nn.ReLU(False),
            nn.Linear(self.embedding_dim // 2, self.embedding_dim),
            nn.ReLU(False),
            nn.Dropout(0.5)
        )

    def forward(self, *inputs):
        x_profile, x_one_hot, x_hhm = inputs

        if not hasattr(self, '_flattened'):
            self.gru1.flatten_parameters()
            setattr(self, '_flattened', True)

        x_merge = torch.cat([x_profile, x_hhm], dim=-1)
        out_mlp2 = self.mlp2(x_merge)
        gru_out, _ = self.gru1(out_mlp2)
        out = self.mlp1(gru_out)

        return out
